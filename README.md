seiton ver 1.2
Author:  Tetsutaro Sumiyoshi (tetsutsum@gmail.com)
Written: 28th July 2017
Last-update: 16th Sep 2017 
This script is designed to compare two fastq files like cmpfastq_pe.
(http://compbio.brc.iop.kcl.ac.uk/software/cmpfastq_pe.php)
You can get two common id sequences and two unique id sequences.
Plese send questions and bugs to tetsutsum@gmail.com.


Usage:
	seiton -l LEFT.fastq -r RIGTH.fastq 

OPTIONS:
	-o 	Output_directory [current directory]
	-h	Show this message
[] : initial value

Seiton output 4 files.
	LEFT_READ1_common.fastq
	LEFT_READ1_uniq.fastq
	RIGHT_READ2_common.fastq
	RIGHT_READ2_uniq.fastq

Named "common" fastq files are same id fastq files between input files.
Also, named "uniq" fastq files are unique id fastq files each input files.

Plese send questions and bugs to tetsutsum@gmail.com.

Copyright (c) 2017 t-sumiyoshi All Rights Reserved.
Released under the MIT license
(http://opensource.org/licenses/mit-license.php)
