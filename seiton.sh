#!/bin/bash

########################################################################
#                                                                      #
#        #######  #######  ##### #######   ####    #       #           #
#       #         #          #      #     #    #   # #     #           #
#       #         #          #      #    #      #  #  #    #           #
#        ######   #######    #      #    #      #  #   #   #           #
#              #  #          #      #    #      #  #    #  #           #
#              #  #          #      #     #    #   #     # #           #
#       #######   #######  #####    #      ####    #       #           #
#                                                                      #
########################################################################
#                                                                      #
# seiton ver 1.2                                                       #
# Author:  Tetsutaro Sumiyoshi (tetsutsum@gmail.com)                   #
# Written: 28th July 2017                                              #
# Last-update: 16th Sep 2017                                           #
# This script is designed to compare two fastq files like cmpfastq_pe. #
# (http://compbio.brc.iop.kcl.ac.uk/software/cmpfastq_pe.php)          #
# You can get two common id sequences and two unique id sequences.     #
# Plese send questions and bugs to tetsutsum@gmail.com.                #
#                                                                      #
# Copyright (c) 2017 t-sumiyoshi All Rights Reserved.                  #
# Released under the MIT license                                       #
# (http://opensource.org/licenses/mit-license.php)                     #
#                                                                      #
########################################################################

usage () {
cat <<_ETO_
Usage:
	seiton -l LEFT.fastq -r RIGTH.fastq 

OPTIONS:
	-o 	Output_directory(not required)
	-h	Show this message
_ETO_
}

while getopts "hl:r:o:" OPTION; do
	case $OPTION in
		h)	usage && exit 0 ;;
		l)	InB1=`readlink -f ${OPTARG}`;;
		r)	InB2=`readlink -f ${OPTARG}`;;
		o)	OutD=`readlink -f ${OPTARG}`;;
	esac
done

#Seiton##########

#input-1 and input-2 check#####
#existence check
if [ ! -f ${InB1} ] || [ "${InB1}" = "" ] || [ ! -f ${InB2} ] || [ "${InB2}" = "" ];then
	if [ ! -f ${InB1} ] || [ "${InB1}" = "" ];then
		echo "\"${InB1}\" is not found."
	fi
	if [ ! -f ${InB2} ] || [ "${InB2}" = "" ];then
		echo "\"${InB2}\" is not found."
	fi
	usage
	exit 1
fi
#difference check
if [ ${InB1} = ${InB2} ]; then
	echo "LEFT read and RIGHT read are SAME."
	usage
	exit 1
fi
#extension check(fastq check)
Lab1=`basename ${InB1} | sed 's/^.*\.\([^\.]*\)$/\1/'`
Lab2=`basename ${InB2} | sed 's/^.*\.\([^\.]*\)$/\1/'`
if [ "$Lab1" != "fq" -a "$Lab1" != "fastq" ]; then
	echo "Is LEFT read really fastq file?"
fi
if [ "$Lab2" != "fq" -a "$Lab2" != "fastq" ]; then
	echo "Is RIGHT read really fastq file?"
fi
########

#Start and output check#####
Lab1=`basename ${InB1} | sed 's/\.[^\.]*$//'`
Lab2=`basename ${InB2} | sed 's/\.[^\.]*$//'`

if [ "${OutD}" = "" ]; then
	#OutD is exist
	if [ -f ${Lab1}_READ1_uniq.fastq ] || [ -f ${Lab1}_READ1_common.fastq ] || [ -f ${Lab2}_READ2_uniq.fastq ] || [ -f ${Lab2}_READ2_common.fastq ];then
		echo "Result file(s) is exist."
		exit 1
	fi

	echo "Seiton ver.1.2"
	tmp=`mktemp -d temp_XXXXXX`
else
	#OutD is absent
	if [ -d ${OutD} ]; then
		Lab1="${OutD}/${Lab1}"
		Lab2="${OutD}/${Lab2}"
	else
		echo "\"${OutD}\" is not found."
		exit 1
	fi
	if [ -f ${Lab1}_READ1_uniq.fastq ] || [ -f ${Lab1}_READ1_common.fastq ] || [ -f ${Lab2}_READ2_uniq.fastq ] || [ -f ${Lab2}_READ2_common.fastq ];then
		echo "Result file(s) is exist."
		exit 1
	fi
	echo "Seiton ver.1.0"
	tmp=`mktemp -d ${3}/temp_XXXXXX`
fi
#cancel(trap)
trap 'rm -r ${tmp}; echo "...!?"; echo "Seiton is canceled."; exit 1' 1 2 3 15
########


#prepare######
echo -n "Preparing.."
cat ${InB1} | awk 'NR%4==1' | awk '{print $1}' | cut -d ':' -f 2- |  cut -d '#' -f 1 | nl | LC_ALL=C sort -k 2,2 | tee >( awk '{print $1}' >${tmp}/READ1_sorted2 ) | awk '{print$2}' >${tmp}/READ1_sorted
echo -n "."
cat ${InB2} | awk 'NR%4==1' | awk '{print $1}' | cut -d ':' -f 2- |  cut -d '#' -f 1 | nl | LC_ALL=C sort -k 2,2 | tee >( awk '{print $1}' >${tmp}/READ2_sorted2 ) | awk '{print$2}' >${tmp}/READ2_sorted
echo -n "."
########


#READ number check#####
NumOfTalREAD1=`cat ${tmp}/READ1_sorted2 | wc -l`
NumOfTalREAD2=`cat ${tmp}/READ2_sorted2 | wc -l`
echo -n "."
if [ $NumOfTalREAD1 -eq $NumOfTalREAD2 ]; then
	echo ""
	echo "This files have same number of reads."
	echo "If you want to stop running this program, please type [CTRL]+C."
fi

echo "..done"
########

#Core######
echo -n "Seiton..."
#READ1
diff --new-line-format='' --old-line-format='0 %L' --unchanged-line-format='1 %L' ${tmp}/READ1_sorted ${tmp}/READ2_sorted | awk '{print $1}' > ${tmp}/READ1com
paste ${tmp}/READ1com ${tmp}/READ1_sorted2 | LC_ALL=C sort -n -k 2,2 | awk '{print $1}' > ${tmp}/READ1get
echo -n "."
paste -d "\n" ${tmp}/READ1get ${tmp}/READ1get ${tmp}/READ1get ${tmp}/READ1get > ${tmp}/READ1getall
echo -n "."
paste ${tmp}/READ1getall ${InB1} | tee >( awk '{if($1==0) print }' | cut -f 2- > ${Lab1}_READ1_uniq.fastq ) | awk '{if($1==1) print }' | cut -f 2- > ${Lab1}_READ1_common.fastq
echo -n ".."
#READ2
diff --new-line-format='0 %L' --old-line-format='' --unchanged-line-format='1 %L' ${tmp}/READ1_sorted ${tmp}/READ2_sorted | awk '{print $1}' > ${tmp}/READ2com
echo -n "."
paste ${tmp}/READ2com ${tmp}/READ2_sorted2 | LC_ALL=C sort -n -k 2,2 | awk '{print $1}' > ${tmp}/READ2get

echo -n "."
paste -d "\n" ${tmp}/READ2get ${tmp}/READ2get ${tmp}/READ2get ${tmp}/READ2get > ${tmp}/READ2getall
echo -n "."
paste ${tmp}/READ2getall ${InB2} | tee >( awk '{if($1==0) print }' | cut -f 2- > ${Lab2}_READ2_uniq.fastq ) | awk '{if($1==1) print }' | cut -f 2- > ${Lab2}_READ2_common.fastq
echo -n ".."
########

#calculate read number#####
COM=`awk '{if($1==1) a += $1} END {print a}' ${tmp}/READ1get`
R1=`echo "scale=2; $COM / $NumOfTalREAD1 * 100" | bc` 
R2=`echo "scale=2; $COM / $NumOfTalREAD2 * 100" | bc`
########

#finished!!!!!#####
rm -r ${tmp}
echo ".done"
echo "READ1/READ2-common $NumOfTalREAD1 reads (${R1}%/${R2}%)"
########

##############

